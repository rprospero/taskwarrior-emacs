;;; taskwarrior.el --- Control Task Warrior through emacs -*- lexical-binding: t; -*-

;; Author: Adam Washington <rprospero@gmail.com>
;; Version: 0.1
;; Package-Requires: (json dash)
;; Keywords: processes
;; URL: http://gitlab.com/rprospero/taskwarrior-emacs

;;; Commentary:

;; This package provides a major mode for interacting with task
;; warrior to see the current task list and explore the tasks.
;; Eventually, it will also enable modifying, finishing and andding
;; tasks.

;;; Code:

(defgroup Task-Warrior ()
  "Control TaskWarrior through emacs"
  :group 'external
  :group 'calendar)

(defcustom task-warrior-command
  '("task" "export")
  "The command and arguments to cause task warrior to export json."
  :type '(repeat string)
  :group 'Task-Warrior)

(defcustom task-warrior-formatter
  '((id . #'tw-key-int-formatter)
    (urgency . #'tw-key-float-formatter))
  "The format strings for the different keys in the json export."
  :type '(alist :key-type symbol :value-type function)
  :group 'Task-Warrior)

(defcustom task-warrior-columns
  '((id "id" 4 t )
    (due "due"  18 t )
    (priority "p"   1 t )
    (project "project"   10 t )
    (urgency "urg"  7 t )
    (description "description" 7 t ))
  "The desired keys to collect from task-warrior."
  :type '(repeat (list (sexp :tag "Key") (string :tag "Name") (integer :tag "Width") (boolean :tag "Sort")))
  ;; :type '(repeat (list string integer boolean))
  :group 'Task-Warrior)

(define-derived-mode taskwarrior-mode tabulated-list-mode "TaskWarrior"
  "Major Mode for Task Warrior"
  (setq tabulated-list-format (apply #'vector (mapcar #'cdr task-warrior-columns)))
  ;; (make-local-variable 'process-menu-query-only)
  (setq tabulated-list-padding 1)
  (setq tabulated-list-sort-key (cons "project" t))
  (setq tabulated-list-revert-hook #'tw-update-tasks)
  (setq tabulated-list-printer #'tw-list-printer)
  (bind-key "RET" 'tw-list-task taskwarrior-mode-map)
  (tabulated-list-init-header))

(define-derived-mode taskwarrior-task-mode tabulated-list-mode "TaskWarrior-Task"
  "Major Mode for Task Warrior Tasks"
  (setq tabulated-list-format [("Name" 15 t)
			       ("Value" 7 t)])
  ;; (make-local-variable 'process-menu-query-only)
  (setq tabulated-list-padding 1)
  (setq tabulated-list-sort-key (cons "Name" t))
  ;; (setq tabulated-list-revert-hook #'tw-update-tasks)
  (tabulated-list-init-header))

(defvar tw--data nil "The current imported data from taskwarrior.")
;;;;;

(defun tw-update-tasks ()
  "Get the current state of the tasks from Task Warrior."
  (make-process :name "task" :buffer "*task-export*"  :command task-warrior-command
	      :sentinel #'tw-handle-task-export))

(defun tw-parse-task (x)
  "Parse a single taskwarrior entry X into the 'tabulated-list-entries' format."
  (list
   (alist-get 'uuid x)
   (apply
    #'vector
    (mapcar
     ;; (lambda (entry) (format "%s" (alist-get (car entry) x "")))
     (lambda (entry) (alist-get (car entry) x ""))
     task-warrior-columns))))

(defun tw-key-string-formatter (value width)
  (format (format "%%%ds" width) value))

(defun tw-key-float-formatter (value width)
  (format (format "%%%d.2f" (- width 3)) value))

(defun tw-key-int-formatter (value width)
  (format (format "%%%dd" width) value))

(defun tw-list-printer (id contents)
  (mapcar*
   (lambda (col val)
     (let ((width (nth 2 col)))
       (insert
	(substring
	 (funcall (alist-get (car col) task-warrior-formatter #'tw-key-string-formatter)
	  val width)
	 0
	 (if (> width 0) (+ 0 width) 'nil)))
       (insert " ")))
   task-warrior-columns
   contents)
  (newline))


(defun tw-handle-task-export (proc state)
  "Parse the current task STATE returned by PROC and update the buffer."
  (with-current-buffer "*task-export*"
    (goto-char (point-min))
    (setq tw--data
	  (seq-filter
	   (lambda (x)
	     (not
	      (or
	       (string-equal (alist-get 'status x) "completed")
	       (string-equal (alist-get 'status x) "deleted"))))
	   (json-read)))
      (message "Read the Data!")
      (with-current-buffer "*Tasks*"
	(setq tabulated-list-entries (mapcar #'tw-parse-task tw--data))
	(tabulated-list-print t))))

(defun tw-update-task (uuid)
  "Display a single task with a given UUID."
  (interactive)
  (let
      ((node
	(seq-find
	 (lambda (x)
	   (string-equal (alist-get 'uuid x) uuid))
	 tw--data)))
    (setq tabulated-list-entries
	  (seq-map
	   (lambda (x)
	     (list nil
		   (vector (format "%s" (car x)) (format "%s" (cdr x)))))
	   node))
    (tabulated-list-print t)))

(defun list-tasks ()
  "Display the current tasks know to task warrior."
  (interactive)
  (pop-to-buffer "*Tasks*" nil)
  (taskwarrior-mode)
  (tw-update-tasks))

(defun tw-list-task ()
  "Load up the info for a single task."
  (interactive)
  (let ((uuid (tabulated-list-get-id)))
    (pop-to-buffer "*Task*" nil)
    (taskwarrior-task-mode)
    (tw-update-task uuid)))



;;;###autoload
(provide 'taskwarrior)
;;; taskwarrior.el ends here
